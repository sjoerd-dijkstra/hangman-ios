//
//  SettingsViewController.m
//  Hangman
//
//  Created by Sjoerd Dijkstra on 17-11-14.
//  Copyright (c) 2014 Sjoerd Dijkstra. All rights reserved.
//

#import "SettingsViewController.h"

/*
 A viewcontroller that handles the settings interface.
 
 The view controller serves as the visual for adjusting and saving 
 the different settings in the game to defualts. GameViewController
 uses these settings.
 */

@interface SettingsViewController ()

@end

@implementation SettingsViewController

#pragma mark - Basics

- (void)viewDidLoad {
    [super viewDidLoad];
    // load settings from memory when view appears
    self.defaults = [NSUserDefaults standardUserDefaults];
    [self syncDefaults];
}

/*
 Function to synchronize the objects in the screen to settings from memory
 */
- (void)syncDefaults {
    // load defaults
    self.EvilSwitch.on   = [self.defaults boolForKey:@"modeEvil"];
    self.GodSwitch.on    = [self.defaults boolForKey:@"modeGod"];
    NSInteger lengthWord = [self.defaults integerForKey:@"lenghtWord"];
    NSInteger maxGuesses = [self.defaults integerForKey:@"maxGuesses"];
    
    // initialze settings screen
    self.WordLengthSlider.maximumValue = [self.defaults integerForKey:@"maxLengthWord"];
    self.WordLengthSlider.value        = lengthWord;
    self.WordLengthLabel.text          = [NSString stringWithFormat:@"%ld", (long)lengthWord];
    self.GuessesSlider.value           = maxGuesses;
    self.GuessesLabel.text             = [NSString stringWithFormat:@"%ld", (long)maxGuesses];
}


#pragma mark - Setting the sliders

/*
 Actions to let the player adjust the prefered word length and amount of guesses.
 */

// changes the word length
- (IBAction)changeWordLength:(id)sender {
    int wordLength = (int) self.WordLengthSlider.value;
    [self.defaults setInteger:wordLength forKey:@"lenghtWord"];
    [self.defaults synchronize];
    self.WordLengthLabel.text = [NSString stringWithFormat:@"%d", wordLength];
}

// changes the amount of 'lives' the player has
- (IBAction)changeGuesses:(id)sender {
    int maxGuesses = (int) self.GuessesSlider.value;
    [self.defaults setInteger:maxGuesses forKey:@"maxGuesses"];
    [self.defaults synchronize];
    self.GuessesLabel.text = [NSString stringWithFormat:@"%d", maxGuesses];
}

#pragma mark - Setting the game style

/*
 Actions to let the player adjust the prefered game style between Evil, 
 God and Normal (if the other ar not selected). Evil mode makes it really
 hard for the player because it always wants as many words as possible to
 choose between. In god mode the computer the computer looks first for all
 words with the chosen letter.
 */

// evil
- (IBAction)changeEvil:(id)sender {
    if (self.EvilSwitch.on) {
        [self.defaults setBool:YES forKey:@"modeEvil"];
        [self.defaults setBool:NO forKey:@"modeGod"];
        self.GodSwitch.on = NO;
    }
    else
        [self.defaults setBool:NO forKey:@"modeEvil"];
    [self.defaults synchronize];
}

// god
- (IBAction)changeGod:(id)sender {
    if (self.GodSwitch.on) {
        [self.defaults setBool:NO forKey:@"modeEvil"];
        [self.defaults setBool:YES forKey:@"modeGod"];
        self.EvilSwitch.on = NO;
    }
    else
        [self.defaults setBool:NO forKey:@"modeGod"];
    [self.defaults synchronize];
}

@end
