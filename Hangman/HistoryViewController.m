//
//  HistoryViewController.m
//  Hangman
//
//  Created by Sjoerd Dijkstra on 17-11-14.
//  Copyright (c) 2014 Sjoerd Dijkstra. All rights reserved.
//

#import "HistoryViewController.h"

/*
 A viewcontroller that handles the highscore interface.
 
 The view controller serves as the visual for seeing the
 history of the top seven players in the three different
 modes.
 */

@interface HistoryViewController ()

@end

@implementation HistoryViewController

#pragma mark - Basics

- (void)viewDidLoad {
    [super viewDidLoad];
    // load history into labels upon first load
    [self refreshHistory];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    // refresh labels when screen appears again
    [self refreshHistory];
}

/*
 This function gets the history array out the default memory that is made in the
 GameViewController. It let's the labels show the content of the array based on
 the segmented control toggle that is located in the bottom of the screen.
 */
- (void) refreshHistory {
    
    // prapre to make highscore labels
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    self.historyMode         = [defaults objectForKey:@"historyMode"];
    NSArray *history         = [defaults objectForKey:@"highScoreHistory"];
    NSString *tempNameLabel  = @"";
    NSString *tempScoreLabel = @"";
    NSInteger namesRank      = 0;
    
    // write highscor labels
    for (NSArray *line in history){
        if ([[line valueForKey:@"mode"] isEqualToString:self.historyMode]) {
            namesRank ++;
            tempNameLabel  = [NSString stringWithFormat: @"%@%ld. %@\n\n",
                              tempNameLabel, (long) namesRank, [line valueForKey:@"name"]];
            tempScoreLabel = [NSString stringWithFormat: @"%@%@\n\n",
                              tempScoreLabel, [line valueForKey:@"score"]];
            if (namesRank == 7)
                break;
        }
    }
    
    // put highs score labels with linebreaks in screen
    self.nameLabel.numberOfLines  = 0;
    self.scoreLabel.numberOfLines = 0;
    self.nameLabel.text  = tempNameLabel;
    self.scoreLabel.text = tempScoreLabel;
    
    // set the segmented controlor object
    if ([self.historyMode isEqual: @".NORMAL."])
        [self.historySegment setSelectedSegmentIndex:0];
    else if ([self.historyMode isEqual: @".GOD."])
        [self.historySegment setSelectedSegmentIndex:1];
    else if ([self.historyMode isEqual: @".EVIL."])
        [self.historySegment setSelectedSegmentIndex:2];
}

#pragma mark - The mode toggle

/*
 The segmented control in the button of the screen lets the 
 users see the high scores in the different modes.
 */
- (IBAction)changeHistoryMode:(id)sender {
    
    // safe to defaults
    NSUserDefaults *defaults    = [NSUserDefaults standardUserDefaults];
    if (self.historySegment.selectedSegmentIndex == 0)
        [defaults setObject:@".NORMAL." forKey:@"historyMode"];
    else if (self.historySegment.selectedSegmentIndex == 1)
        [defaults setObject:@".GOD." forKey:@"historyMode"];
    else if (self.historySegment.selectedSegmentIndex == 2)
        [defaults setObject:@".EVIL." forKey:@"historyMode"];
    [defaults synchronize];
    
    // refresh screen
    [self refreshHistory];
}
@end
