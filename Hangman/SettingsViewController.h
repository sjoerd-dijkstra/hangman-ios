//
//  SettingsViewController.h
//  Hangman
//
//  Created by Sjoerd Dijkstra on 17-11-14.
//  Copyright (c) 2014 Sjoerd Dijkstra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsViewController : UIViewController

@property (strong, nonatomic) NSUserDefaults *defaults;

@property (weak, nonatomic) IBOutlet UISwitch *EvilSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *GodSwitch;
@property (weak, nonatomic) IBOutlet UILabel *WordLengthLabel;
@property (weak, nonatomic) IBOutlet UILabel *GuessesLabel;
@property (weak, nonatomic) IBOutlet UISlider *WordLengthSlider;
@property (weak, nonatomic) IBOutlet UISlider *GuessesSlider;

- (IBAction)changeWordLength:(id)sender;
- (IBAction)changeGuesses:(id)sender;
- (IBAction)changeEvil:(id)sender;
- (IBAction)changeGod:(id)sender;

@end

