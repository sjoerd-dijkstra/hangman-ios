# Design Document
The design document conveys the way the app fits into the platform it is build in and the specific design choices that are made.

### GameController
* `EvilPlay`: Algorithm used to play an unfair hangman game.
* `NormalPlay`: model used to play a fair hangman game. (if time alows)

### TabController
* `MainViewController`
* `RestartViewController`
* `SettingsViewController`
* `HistoryViewController`

### MainViewController
* `TitleLabel`: Label 
* `LetterPlaceholders`: Label
* `GuessesLeft`: Progressbar
* `AlphabetUsed`: Label
* `TextField`: To input letters

### RestartViewController
* `Yes`: Button To restart game and switch to first tab

### SettingsViewController
* `EvilMode`: Switch to delegate the GameController
* `WordLengthLabel`: represents length of word
* `WordLengthSlider`: controls length of word
* `GuessesLabel`: represents amount of guesses
* `GuessesSlider`: controls amount of guesses

### HistoryViewController
* `Top5-Label` shows best scores


## Database
The contents of `words.plist` are the universe of possible words.

## References
[Readme](https://github.com/Sjoerdie/Hangman/blob/master/README.md)
