//
//  HistoryViewController.h
//  Hangman
//
//  Created by Sjoerd Dijkstra on 17-11-14.
//  Copyright (c) 2014 Sjoerd Dijkstra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HistoryViewController : UIViewController

@property (weak, nonatomic) NSString *historyMode;
@property (weak, nonatomic) IBOutlet UISegmentedControl *historySegment;

@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

- (IBAction)changeHistoryMode:(id)sender;

@end
