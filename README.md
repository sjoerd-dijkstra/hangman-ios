# Readme
Evil Hangman is quite like Hangman, except that the computer cheats.

## Table of Contents
* [Compiling](#compiling)
* [Features](#features)
* [References](#references)

## Compiling
The program is written in `Objective-C` for `iOS 8.1` with `Xcode 6`.

## Features
* `UIViewControllers`:
	* `UIPageViewController` for pageflip animation (serves as root)
	* `GameViewController` for game interface (serves as mainview)
	* `SettingsViwController` for adjusting settings
	* `HistoryViewController` to see highscores in defferent playmodes
* `GameController` serves as algorithm protocol to switch between playmodes
	* `EVIL MODE`: computers tries to win
	* `GOD MODE`: computer tries to let you win
	* `NORMAL MODE`: computer selects a randomn word

## References
* [Design document](https://github.com/Sjoerdie/Hangman-iOS/blob/master/doc/Design%20document.md)
* [Style guide](https://github.com/Sjoerdie/Hangman-iOS/blob/master/doc/Style%20guide.md)
* [Icon, by Vlad Marin](https://www.iconfinder.com/iconsets/brain-games)
