//
//  PageViewController.h
//  Hangman
//
//  Created by Sjoerd Dijkstra on 01-12-14.
//  Copyright (c) 2014 Sjoerd Dijkstra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GameViewController.h"
//@class GameViewController;
@class SettingsViewController;
@class HistoryViewController;

@interface PageViewController : UIViewController <UIPageViewControllerDataSource>

@property (strong, nonatomic) UIPageViewController *pageController;
@property (nonatomic, strong) NSArray *contentViewControllers;

@end

