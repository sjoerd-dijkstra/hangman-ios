//
//  GameController.m
//  Hangman
//
//  Created by Sjoerd Dijkstra on 21-11-14.
//  Copyright (c) 2014 Sjoerd Dijkstra. All rights reserved.
//

#import "GameController.h"

/*
 A controller object that manages the game algorithm - Hangman.
 
 The controller serves as the words array modifier based on the
 letter that is given and the mode that is selected by the user.
 
 The GameController functions in relation to GameViewController.
 */

@implementation GameController

# pragma mark - Main function

// returns the playwords to player that uses the GameViewController
- (NSMutableArray*)hangmanPlayWithLetter:(NSString *)       playLetter
                                   Words:(NSMutableArray *) playWords
                              WordLength:(NSInteger)        playLength
                                PlayMode:(NSString *)       playMode {
    
    // prepare global variables for the algorithms
    self.hangmanWords = [NSMutableArray new];
    self.wordClasses  = [NSMutableDictionary new];
    self.lengthWord   = playLength;
        
    // return input when only word left
    if (playWords.count == 1)
        return playWords;

    // god mode: select all the words that contain the letter
    if ([playMode containsString: @"GOD"]) {
        
        // add all words that contain letter to the global array
        for (NSString *word in playWords)
            if ([[word uppercaseString] containsString:playLetter])
                [self.hangmanWords addObject:[[NSMutableString alloc] initWithFormat:@"%@", word]];
        
        // forward to evil algorithm or return input if there are no words with the letter
        if (self.hangmanWords.count)
            playWords = self.hangmanWords;
        else
            return playWords;
    }
    
    // evil mode: seacrh biggest word class to return to player
    if ([playMode containsString: @"EVIL"] | [playMode containsString: @"GOD"]) {
        
        // prepare, hash and store the words into a dictionary
        NSInteger hash;
        for (NSString *word in playWords){
            hash = [self hashForWord:[word uppercaseString] AndLetter:playLetter];
            [self putWord:word InDictionaryWithHash:hash];
        }
        
        // add the words from the biggest word class to the global array
        [self largestClassInDictionary];
    }
    
    // normal mode: choose random word
    else if (playWords.count > 1)
        [self randomWordFromArray:playWords];
    
    // thats it :)
    return self.hangmanWords;
}

# pragma mark - Helper Functions for hangmanPlay

- (NSInteger)hashForWord:(NSString *)word AndLetter:(NSString *)playLetter {
    
    // prepare
    NSInteger hash = 0;
    
    // calculate the hash based on multiplication of the letterposition
    if ([word containsString:playLetter])
        for (int letterPosition = 0; letterPosition < self.lengthWord; letterPosition++)
            if ([[word substringWithRange: NSMakeRange(letterPosition, 1)] isEqualToString:playLetter])
                hash += (NSInteger)pow(2, letterPosition + 1);
    
    // return hash
    return hash;
}

- (void)putWord:(NSString *)word InDictionaryWithHash:(NSInteger )hash {
    
    // prepare key and 'set' or 'add' the word into the global dicitonary
    NSString *stringKey = [NSString stringWithFormat:@"%ld", (long)hash];
    if ([self.wordClasses objectForKey:stringKey])
        [[self.wordClasses objectForKey:stringKey] addObject:word];
    else
        [self.wordClasses setObject:[[NSMutableArray alloc] initWithObjects:word, nil] forKey:stringKey];
}

- (void)largestClassInDictionary {
    
    // prepare to find largest class
    NSInteger maxSize = 0;
    NSString *stringKey;
    
    // go find largest class
    for (id key in self.wordClasses) {
        if ((NSInteger) [[self.wordClasses objectForKey:key] count] > maxSize){
            maxSize = [[self.wordClasses objectForKey:key] count];
            stringKey = key;
        }
    }
    
    // add the content from the lergest class in the global dictionary to the global array
    self.hangmanWords = [self.wordClasses objectForKey:stringKey];
}

- (void)randomWordFromArray:(NSMutableArray *)playWords {
    // get a random number and add the one word to the global array
    NSInteger KEY = arc4random_uniform([playWords count] - 1.0);
    [self.hangmanWords addObject:[[NSMutableString alloc] initWithFormat:@"%@", playWords[KEY]]];
}

@end
