//
//  GameController.h
//  Hangman
//
//  Created by Sjoerd Dijkstra on 21-11-14.
//  Copyright (c) 2014 Sjoerd Dijkstra. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GameController : NSObject

@property (nonatomic, strong) NSMutableDictionary *wordClasses;
@property (nonatomic, strong) NSMutableArray *hangmanWords;
@property (nonatomic, assign) NSInteger lengthWord;

- (NSMutableArray*)hangmanPlayWithLetter:(NSString *)       playLetter
                                   Words:(NSMutableArray *) playWords
                              WordLength:(NSInteger)        playLength
                                PlayMode:(NSString *)       playMode;

@end
