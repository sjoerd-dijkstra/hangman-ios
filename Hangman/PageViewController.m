//
//  PageViewController.m
//  Hangman
//
//  Created by Sjoerd Dijkstra on 01-12-14.
//  Copyright (c) 2014 Sjoerd Dijkstra. All rights reserved.
//

#import "PageViewController.h"

/*
 A controller object that manages a simple model -- a collection of viewcontrollers.
 
 The controller serves as the data source for the static page view controller; it 
 therefore implements pageViewController:viewControllerBeforeViewController: and
 pageViewController:viewControllerAfterViewController:.
 */

@interface PageViewController ()

@end

@implementation PageViewController

#pragma mark - Basics

- (void)viewDidLoad
{
    [super viewDidLoad];
 
    
    // instantiate the viewcontrollers
    GameViewController *gameViewController         = [self.storyboard instantiateViewControllerWithIdentifier:@"GameViewController"];
    SettingsViewController *settingsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingsViewController"];
    HistoryViewController *historyViewController   = [self.storyboard instantiateViewControllerWithIdentifier:@"HistoryViewController"];
    self.contentViewControllers = @[gameViewController, settingsViewController, historyViewController];
    
    // Configure the page view controller and add it as a child view controller.
    self.pageController = [[UIPageViewController alloc]
                           initWithTransitionStyle:UIPageViewControllerTransitionStylePageCurl
                             navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal
                                           options:nil];
    
    self.pageController.dataSource = self;
    self.pageController.view.frame = self.view.bounds;
    
    [self.pageController setViewControllers:@[gameViewController]
                                  direction:UIPageViewControllerNavigationDirectionForward
                                   animated:NO
                                 completion:nil];
    
    [self addChildViewController:self.pageController];
    [self.view addSubview:self.pageController.view];
    
    [self.pageController didMoveToParentViewController:self];
    
    // Add the page view controller's gesture recognizers to the book view controller's view so that the gestures are started more easily.
    self.view.gestureRecognizers = self.pageController.gestureRecognizers;
}

#pragma mark - Page View Controller Data Source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController
      viewControllerBeforeViewController:(UIViewController *)viewController {
    
    NSUInteger index = [self.contentViewControllers indexOfObject:viewController];
    
    if (index == 0) {
        return nil;
    }
    return self.contentViewControllers[index - 1];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController
       viewControllerAfterViewController:(UIViewController *)viewController {
    
    NSUInteger index = [self.contentViewControllers indexOfObject:viewController];
    
    if (index >= self.contentViewControllers.count - 1) {
        return nil;
    }
    
    return self.contentViewControllers[index + 1];
}

@end

