//
//  GameViewController.h
//  Hangman
//
//  Created by Sjoerd Dijkstra on 17-11-14.
//  Copyright (c) 2014 Sjoerd Dijkstra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GameController.h"

@interface GameViewController : UIViewController <UIKeyInput>

@property (nonatomic, strong) GameController *gameController;

@property (nonatomic, strong) NSMutableArray *dictionaryWords;
@property (nonatomic, strong) NSMutableArray *hangmanWords;
@property (nonatomic, strong) NSString *hangmanMode;

@property (nonatomic, assign) NSInteger lengthWord;
@property (nonatomic, assign) NSInteger maxGuesses;
@property (nonatomic, assign) NSInteger leftGuesses;
@property (nonatomic, assign) NSInteger totalScore;

@property (weak, nonatomic) IBOutlet UIImageView *hangManIntro;
@property (weak, nonatomic) IBOutlet UIImageView *hangManImage;
@property (weak, nonatomic) IBOutlet UIImageView *introImage;

@property (weak, nonatomic) IBOutlet UILabel *textInputLabel;
@property (weak, nonatomic) IBOutlet UILabel *placeHolders;
@property (weak, nonatomic) IBOutlet UILabel *placeHoldersStatic;
@property (weak, nonatomic) IBOutlet UILabel *letterList;
@property (weak, nonatomic) IBOutlet UILabel *leftGuessesLabel;
@property (weak, nonatomic) IBOutlet UILabel *modeLabel;
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;
@property (weak, nonatomic) IBOutlet UILabel *yourNameLabel;

@property (weak, nonatomic) IBOutlet UIButton *introButton;
@property (weak, nonatomic) IBOutlet UIButton *restartEndGameButton;

- (IBAction)restartSwipe:(id)sender;
- (IBAction)introButton:(id)sender;

@end

