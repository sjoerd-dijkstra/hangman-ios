//
//  GameViewController.m
//  Hangman
//
//  Created by Sjoerd Dijkstra on 17-11-14.
//  Copyright (c) 2014 Sjoerd Dijkstra. All rights reserved.
//

#import "GameViewController.h"

/*
 A viewcontroller that handles the game interface with keybord.
 
 The view controller serves as the visual for gameprogress and
 interaction with the player.
 */

@interface UIViewController ()

@end

@implementation GameViewController

#pragma mark - Basics

- (void)viewDidLoad {
    [super viewDidLoad];
    // allocate game comtroller and start the game when view loaded the first time
    self.gameController = [[GameController alloc] init];
    [self restartSwipe:self];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    // assure keyboard functionality when there are guesses left
    if ([self.introButton isHidden] && (self.leftGuesses > 0))
        [self becomeFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // remove biggest object when there is a memory warning
    [self.dictionaryWords removeAllObjects];
}

/*
 Traditional hangman visual that is used to show the player his/her game progress.
 */
- (void)changeImage {
    // calculate progress percentage determine appropriate image to show
    float progressGuesses = 1 - (float) self.leftGuesses / self.maxGuesses;
    NSString *imageName = [NSString stringWithFormat:@"hangMan%02d.png", (int) (progressGuesses * 16 + 0.5)];
    UIImage *image = [UIImage imageNamed:imageName];
    [self.hangManImage setImage:image];
}

/*
 Custom alert that is used throughout the code to notify the player about things.
 */
- (void)infoText:(NSString *)text withDelay:(float)seconds {
    // prepare
    [self.infoLabel setHidden:NO];
    self.infoLabel.alpha = 1.0;
    self.infoLabel.numberOfLines = 0;
    self.infoLabel.text = text;
    
    // show with animation
    [UIView animateWithDuration:0.5
                          delay:seconds
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^ {
                         self.infoLabel.alpha = 0.0;
                     }
                     completion:^(BOOL finished) {
                         [self.infoLabel setHidden:YES];
                     }];}

#pragma mark - Restart game

/*
 Function protocol to restart game progress upon button event.
 */
- (IBAction)restartSwipe:(id)sender {
    
    // load stuff
    [self loadSettings];
    [self loadDictionary];
    
    // adjust to settings
    [self sortWords];
    [self resetLabels];
    
    // make visuals
    [self changeImage];
    [self objectVisibility];
    [self animation];
    
}

// the buttons' visibility is used to check wheter the player has seen the introscreen.
- (IBAction)introButton:(id)sender {
    [self.introButton setHidden:YES];
    [self restartSwipe:self];
}

// loads word lenght, amount of lives and the mode that should be payed in
- (void)loadSettings {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    self.lengthWord     = [defaults integerForKey:@"lenghtWord"];
    self.maxGuesses     = [defaults integerForKey:@"maxGuesses"];
    self.leftGuesses    = self.maxGuesses;
    
    if ([defaults boolForKey:@"modeEvil"])
        self.hangmanMode = [NSString stringWithFormat:@".EVIL."];
    else if ([defaults boolForKey:@"modeGod"])
        self.hangmanMode = [NSString stringWithFormat:@".GOD."];
    else
        self.hangmanMode = [NSString stringWithFormat:@".NORMAL."];
}

// gets words from .plist into an array
- (void)loadDictionary {
    if (!self.dictionaryWords)
        self.dictionaryWords = [[NSMutableArray alloc]
                                initWithContentsOfFile:[[NSBundle mainBundle]
                                                        pathForResource:@"words"
                                                        ofType:@"plist"]];
}

// gets the words that are length determined by the player (or defaults)
- (void)sortWords {
    // prepare
    self.hangmanWords = [[NSMutableArray alloc] init];
    NSInteger wordLength;
    NSInteger maxWordLength = 0;
    
    // go
    for (NSString *word in self.dictionaryWords) {
        wordLength = word.length;
        if (wordLength > maxWordLength)
            maxWordLength = word.length;
        if (wordLength == self.lengthWord)
            [self.hangmanWords addObject:[[NSMutableString alloc]
                                          initWithFormat:@"%@", word]];
    }
    
    // save longest word length to memory
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setInteger:maxWordLength forKey:@"maxLengthWord"];
    [defaults synchronize];
}



// resets the texts for adjustable labels in the screen
- (void)resetLabels {
    // make placeholders for letters
    NSMutableString *placeholderString = [[NSMutableString alloc] init];
    for (int i = 0; i < self.lengthWord; i++)
        [placeholderString appendString:@"_ "];
    
    // update all objects in screen
    self.placeHolders.text        = placeholderString;
    self.placeHoldersStatic.text  = placeholderString;
    self.letterList.numberOfLines = 0;
    self.letterList.text          = [NSString stringWithFormat:@"ABCDE\nFGHIJ\nKLMNO\nPQRST\nUVWXY\nZ 012\n34567\n89 -"];
    self.leftGuessesLabel.text    = [NSString stringWithFormat:@"Lives:%ld", (long)self.leftGuesses];
    self.modeLabel.text           = self.hangmanMode;
    self.textInputLabel.text      = @"";
}

// makes objects in screen visible based on the players' skills
- (void)objectVisibility {
    // prepare bools
    NSInteger beginner;
    NSInteger advanced;
    
    // determine skills player
    if (![[[NSUserDefaults standardUserDefaults] objectForKey:@"highScoreHistory"] count] && ![self.introButton isHidden]) {
        beginner = 1;
        advanced = 0;
        [self resignFirstResponder];
    }
    else {
        beginner = 0;
        advanced = 1;
        [self infoText:@"Guess the word!\n\nGood luck!" withDelay:4.0];
    }
    
    // hide or show objects based on skills player
    [self.introImage         setHidden:advanced];
    [self.introButton        setHidden:advanced];
    [self.infoLabel          setHidden:beginner];
    [self.leftGuessesLabel   setHidden:beginner];
    [self.modeLabel          setHidden:beginner];
    [self.placeHolders       setHidden:beginner];
    [self.placeHoldersStatic setHidden:beginner];
    [self.letterList         setHidden:beginner];
    [self.textInputLabel     setHidden:beginner];
    
    // cartoon always visible when game starts
    [self.hangManIntro setHidden:NO];
    
    // always hidden till end of game
    [self.restartEndGameButton setHidden:YES];
    [self.yourNameLabel        setHidden:YES];
}

// make a nice 'new paper' animation to start the new game
- (void)animation {
    [UIView transitionWithView:[self view]
                      duration:1
                       options:UIViewAnimationOptionTransitionCurlUp
                    animations:nil
                    completion:^(BOOL finished){
                        if (finished)
                            // make sure keyboard is functional
                            if ([self.introButton isHidden])
                                [self becomeFirstResponder];
                    }];
}

#pragma mark - Play letter

/*
 Function protocol to play a letter
 */
- (void)play {
    
    // hide cartoon
    [self.hangManIntro setHidden:YES];
    
    // get letter
    NSString *letter = self.textInputLabel.text;
    
    // alert for used letter(s)
    if (![self.letterList.text containsString:letter]) {
        [self infoText:@"Already\nused!" withDelay:1.5];
        return;
    }
    
    // involve game algorithm with words array
    self.hangmanWords = [self.gameController hangmanPlayWithLetter:letter
                                                             Words:self.hangmanWords
                                                        WordLength:self.lengthWord
                                                          PlayMode:self.hangmanMode];
    
    NSLog(@"%@", self.hangmanWords);
    
    // check if the letter is actually in the word
    [self letterInWord:letter];
    
    // check if the end of the game is reached
    [self winOrLose];
    
}

// checks wheter the player made a good or wrong guess
- (void)letterInWord:(NSString *)letter {
    if ([[self.hangmanWords[0] uppercaseString] containsString: letter]){
        // upon good guess: give alert and show the letter(s)
        [self infoText:@"Yes!" withDelay:0.5];
        for (int i = 0; i < self.lengthWord; i++)
            if ([[[self.hangmanWords[0] substringWithRange: NSMakeRange(i,1)] uppercaseString] isEqualToString:letter])
                self.placeHolders.text = [self.placeHolders.text
                                          stringByReplacingCharactersInRange:NSMakeRange(i * 2, 1)
                                          withString:letter];
    }
    else {
        // upon wrong guess: give alert, update lives and image
        [self infoText:@"No!" withDelay:0.5];
        self.leftGuesses--;
        self.leftGuessesLabel.text = [NSString stringWithFormat:@"Lives:%ld", (long)self.leftGuesses];
        [self changeImage];
    }
    
    // always remove letter from letterList
    self.letterList.text = [self.letterList.text stringByReplacingOccurrencesOfString: letter withString:@" "];
}

// checks if the player won or lost the game
- (void)winOrLose {
    if (![self.placeHolders.text containsString:@"_"]){
        // upon win: calculate total score and initialize winningscreen
        self.totalScore      = 100 - self.leftGuesses;
        self.letterList.text = [NSString stringWithFormat:@"\n\nYou win!\n\nScore:\n%ld", (long)self.totalScore];
        [self.leftGuessesLabel     setHidden:YES];
        [self.modeLabel            setHidden:YES];
        [self.yourNameLabel        setHidden:NO];
        [self.restartEndGameButton setHidden:NO];
    }
    if (self.leftGuesses == 0) {
        // upon lose: init lose screen with no more keyboard functionality and word
        self.letterList.text = [NSString stringWithFormat:@"\n\nYou\nlose!\n\nAnswer:"];
        for (int i = 0; i < self.lengthWord; i++)
            self.placeHolders.text = [self.placeHolders.text
                                          stringByReplacingCharactersInRange:NSMakeRange(i * 2, 1)
                                          withString:[[self.hangmanWords[0] substringWithRange: NSMakeRange(i,1)] uppercaseString]];
        [self.restartEndGameButton setHidden:NO];
        [self resignFirstResponder];
    }
}

#pragma mark - Submit history

/*
 Function to get name and score in dictionary objects in nicely sorted array
 */
- (void)submit {
    
    // get name from user input
    NSString *name = self.textInputLabel.text;
    [self resignFirstResponder];
    
    // make disctionary object for player with all relevant info
    NSMutableDictionary *nameAndScore =[[NSMutableDictionary alloc] init];
    [nameAndScore setObject:name forKey:@"name"];
    [nameAndScore setObject:[NSString stringWithFormat:@"%ld", (long)self.totalScore] forKey:@"score"];
    [nameAndScore setObject:self.hangmanMode forKey:@"mode"];
    [nameAndScore setObject:[NSString stringWithFormat:@"%ld", (long)self.leftGuesses] forKey:@"leftGuesses"];
    [nameAndScore setObject:[NSString stringWithFormat:@"%ld", (long)self.maxGuesses] forKey:@"maxGuesses"];
    [nameAndScore setObject:[NSString stringWithFormat:@"%ld", (long)self.lengthWord] forKey:@"lentghWord"];
    
    // add the object to a temporary copy from the default history
    NSUserDefaults *defaults    = [NSUserDefaults standardUserDefaults];
    NSMutableArray *tempHistory = [[defaults arrayForKey:@"highScoreHistory"] mutableCopy];
    [tempHistory addObject:nameAndScore];
    
    // sort the dictionary descending upon play mode and score
    NSSortDescriptor *sortDescriptorScore = [[NSSortDescriptor alloc] initWithKey:@"score" ascending:NO];
    NSSortDescriptor *sortDescriptorMode = [[NSSortDescriptor alloc] initWithKey:@"mode" ascending:YES];
    NSArray *sortDescriptors = @[sortDescriptorScore, sortDescriptorMode];
    NSArray *sortedArray;
    sortedArray = [tempHistory sortedArrayUsingDescriptors:sortDescriptors];
    
    // update the history memory
    [defaults setObject:sortedArray forKey:@"highScoreHistory"];
    [defaults synchronize];
    
    // empty lives to disallow high-score hack bug
    self.leftGuesses = 0;
    
    // thank player for input
    [self infoText:@"Thank\nyou!\nNew Game?" withDelay:2.5];
}

#pragma mark - Keyboard features

/*
 The GameViewController uses the self as first responder to the keyboard
 */

// Bools
- (BOOL)canBecomeFirstResponder       {return YES;}
- (BOOL)enablesReturnKeyAutomatically {return YES;}

- (BOOL)hasText {
    // check for text to use the auto-return key above
    if ([self.textInputLabel.text isEqualToString:@""])
        return NO;
    else
        return YES;
}

// Appearance
- (UIReturnKeyType)returnKeyType               {return UIReturnKeyGo;}
- (UITextAutocorrectionType)autocorrectionType {return UITextAutocorrectionTypeNo;}
- (UIKeyboardAppearance)keyboardAppearance     {return UIKeyboardAppearanceDark;}
- (UITextSpellCheckingType)spellCheckingType   {return UITextSpellCheckingTypeNo;}

/*
 The the text handling protocol
 */
- (void)insertText:(NSString *)letter {
    
    
    if ([letter isEqualToString:@"\n"]) {
        // use return key to trigger the 'play' or 'submit' functions
        if ([self.placeHolders.text containsString:@"_"])
            [self play];
        else
            [self submit];
        self.textInputLabel.text = @"";
    }
    else if ([self.placeHolders.text containsString:@"_"]) {
        // for the play function the player sees a uppercase alphabetic letter
        letter = [letter uppercaseString];
//        NSRange letterRange = NSMakeRange('A', 26) ;
//        NSCharacterSet *letterSet = [NSCharacterSet characterSetWithRange:letterRange];
//        letterSet = [letterSet invertedSet];
//        text = [text stringByTrimmingCharactersInSet:letterSet];
        self.textInputLabel.text = letter;
//        [self play];
    }
    else {
        // for the submit function the player sees his/her name
        [self.yourNameLabel setHidden:YES];
        if (self.textInputLabel.text.length != 0)
            letter = [NSString stringWithFormat:@"%@%@", self.textInputLabel.text, letter];
        if (self.textInputLabel.text.length <= 12)
            self.textInputLabel.text = letter;
    }
}

// handy trick to enable deletion
- (void)deleteBackward {
    if (self.textInputLabel.text.length > 0)
        self.textInputLabel.text = [self.textInputLabel.text substringToIndex:self.textInputLabel.text.length - 1];
    else
        self.textInputLabel.text = @"";
}

@end
